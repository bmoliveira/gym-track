# ActivitySession

A session/series of workouts.

## Properties

---

##### workouts: List\<Workout>

##### start_date: Date

##### end_date: Date
