# Fitnapp

This application will be the companion and the logger for fitness session and exercises.

For detailed information check the [Specs](/documentation/specs).

## Getting Started

This is a flutter application to setup the project just follow the guide on the [official documentation](https://flutter.io/).

## Documentation

The documentation of the app can be found on de [Documentation](/documentation/) folder.

There you can find the specs and the roadmap of the future.
